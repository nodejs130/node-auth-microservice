const express = require('express');

const response = require('../network/response');
const store = require('../store/mysql');

const router = express.Router();

router.get('/:table', async (req, res, next) => {
  try {
    const data = await store.list(req.params.table);
    response.success(req, res, data, 200);
  } catch (error) {
    next(error);
  }
});

router.get('/:table/:id', async (req, res, next) => {
  try {
    const data = await store.get(req.params.table, req.params.id);
    response.success(req, res, data, 200);
  } catch (error) {
    next(error);
  }
});

router.post('/:table', async (req, res, next) => {
  try {
    const data = await store.upsert(req.params.table, req.body);
    response.success(req, res, data, 201);
  } catch (error) {
    next(error);
  }
});

router.post('/:table/query', async (req, res, next) => {
  try {
    const { table } = req.params;
    const { query, join } = req.body;
    const data = await store.query(table, query, join);
    response.success(req, res, data, 200);
  } catch (error) {
    next(error);
  }
});

router.put('/:table', async (req, res, next) => {
  try {
    const data = await store.upsert(req.params.table, req.body);
    response.success(req, res, data, 200);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
