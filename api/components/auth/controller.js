const bcrypt = require('bcrypt');
const auth = require('../../../auth');

const TABLE = 'auth';

module.exports = function (injectedStore) {
  let store = injectedStore;

  if (!store) {
    const store = require('../../../store/dummy');
  }

  async function login(username, password) {
    const result = await store.query(TABLE, { username });
    const data = JSON.parse(JSON.stringify(result[0]));

    const validate = await bcrypt.compare(password, data.password);

    if (validate === true) {
      // Generar token
      return auth.sign(data);
    } else {
      throw new Error('Informacion invalida');
    }
  }

  async function upsert(data) {
    const authData = {
      id: data.id,
    };

    if (data.username) {
      authData.username = data.username;
    }

    if (data.password) {
      authData.password = await bcrypt.hash(data.password, 7);
    }

    return store.upsert(TABLE, authData);
  }

  return {
    upsert,
    login,
  };
};
