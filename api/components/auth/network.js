const express = require('express');

const response = require('../../../network/response');
const controller = require('./index');

const router = express.Router();

router.post('/login', async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const token = await controller.login(username, password);
    response.success(req, res, token);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
