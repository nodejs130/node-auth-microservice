const express = require('express');

const secure = require('./secure');
const response = require('../../../network/response');
const controller = require('./index');

const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const list = await controller.list();
    response.success(req, res, list);
  } catch (error) {
    next(error);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const user = await controller.get(req.params.id);
    response.success(req, res, user);
  } catch (error) {
    next(error);
  }
});

router.get('/:id/following', async (req, res, next) => {
  try {
    const data = await controller.following(req.params.id);
    response.success(req, res, data);
  } catch (error) {
    next(error);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const user = await controller.upsert(req.body);
    response.success(req, res, user, 201);
  } catch (error) {
    next(error);
  }
});

router.post('/follow/:id', secure('follow'), async (req, res, next) => {
  try {
    const data = await controller.follow(req.user.id, req.params.id);
    response.success(req, res, data, 201);
  } catch (error) {
    next(error);
  }
});

router.put('/', secure('update'), async (req, res, next) => {
  try {
    const user = await controller.upsert(req.body);
    response.success(req, res, user, 201);
  } catch (error) {
    next(error);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
    await controller.remove(req.params.id);
    response.success(req, res, 'usuario eliminado');
  } catch (error) {
    next(error);
  }
});

module.exports = router;
