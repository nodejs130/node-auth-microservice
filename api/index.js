require('dotenv').config();

const express = require('express');

const config = require('../config.js');
const bodyParser = require('body-parser');

const user = require('./components/user/network');
const auth = require('./components/auth/network');
const errors = require('../network/errors');

const app = express();

// Settings
app.use(bodyParser.json());

// Router
app.use('/api/user', user);
app.use('/api/auth', auth);

// Errors middleware: Always needs to be placed at last
app.use(errors);

app.listen(config.api.port, () => {
  console.log(`Api escuchando en http://localhost:${config.api.port}`);
});
