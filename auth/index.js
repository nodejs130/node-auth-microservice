const jwt = require('jsonwebtoken');
const config = require('../config');
const error = require('../utils/error');

const secret = config.jwt.secret;

function sign(data) {
  return jwt.sign(data, secret);
}

function verifyToken(token) {
  return jwt.verify(token, secret);
}

const check = {
  own: function (req, owner) {
    const token = decodeHeader(req);
    console.log(token);

    // Comprobar SI es o NO Propio
    if (token.id !== owner) {
      throw error('No puedes realizar esta accion', 401);
    }
  },
  logged: function (req) {
    const token = decodeHeader(req);
  },
};

function getToken(auth) {
  if (!auth) {
    throw error('No se encuentra el token', 401);
  }

  if (auth.indexOf('Bearer ') === -1) {
    throw error('Formato de token invalido', 401);
  }

  let token = auth.replace('Bearer ', '');
  return token;
}

function decodeHeader(req) {
  const authorization = req.headers.authorization || '';
  const token = getToken(authorization);
  const decoded = verifyToken(token);

  req.user = decoded;

  return decoded;
}

module.exports = {
  sign,
  check,
};
