const express = require('express');

const response = require('../../../network/response');
const controller = require('./index');

const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const list = await controller.list();
    response.success(req, res, list);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
