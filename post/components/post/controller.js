const TABLE = 'post';

module.exports = function (injectedStore) {
  let store = injectedStore;

  if (!store) {
    const store = require('../../../store/dummy');
  }

  function list() {
    return store.list(TABLE);
  }

  return {
    list,
  };
};
