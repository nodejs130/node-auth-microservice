require('dotenv').config();

const express = require('express');

const config = require('../config.js');
const bodyParser = require('body-parser');

const post = require('./components/post/network');
const errors = require('../network/errors');

const app = express();

// Settings
app.use(bodyParser.json());

// Router
app.use('/api/post', post);

// Errors middleware: Always needs to be placed at last
app.use(errors);

app.listen(config.post.port, () => {
  console.log(`Servicio post escuchando en http://localhost:${config.post.port}`);
});
