const request = require('request');

function createRemoteDB(host, port) {
  const URL = `http://${host}:${port}`;

  function list(table) {
    return req('GET', table);
  }

  function get(table, id) {
    return req('GET', `${table}/${id}`);
  }

  async function upsert(table, data) {
    let exists = await get(table, data.id);

    if (exists) {
      return req('PUT', table, data);
    } else {
      return req('POST', table, data);
    }
  }

  function query(table, query, join = {}) {
    return req('POST', `${table}/query`, { query, join });
  }

  function req(method, table, data) {
    let url = `${URL}/${table}`;
    console.log(url);
    body = data ? JSON.stringify(data) : '';

    return new Promise((resolve, reject) => {
      request(
        {
          method,
          headers: {
            'content-type': 'application/json',
          },
          url,
          body,
        },
        (e, req, body) => {
          if (e) {
            console.error('Error con la BD remota: ', e);
            return reject(e);
          }

          const resp = JSON.parse(body);
          return resolve(resp.body);
        }
      );
    });
  }

  return {
    list,
    get,
    upsert,
    query,
  };
}

module.exports = createRemoteDB;
